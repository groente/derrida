#!/usr/bin/python3

import requests
import json
import sys
import argparse
import re
import csv
import os
from html.parser import HTMLParser
from html.entities import name2codepoint


def zoek(auteur,titel):
    auteur = re.sub(' ','+',auteur)
    titel = re.sub(' ','+',titel)

    url = 'https://www.delpher.nl/nl/pres/results/multi?coll=boeken&cql%5B%5D=(title+all+%22' + titel + '%22)&cql%5B%5D=(creator+all+%22' + auteur + '%22)&actions%5B%5D=results&actions%5B%5D=paginate&actions%5B%5D=resultsCount&actions%5B%5D=breadcrumbs&actions%5B%5D=sortlinks&actions%5B%5D=facets&actions%5B%5D=facettoggle'

    r = requests.get(url)
    if r.status_code == 200:
        try:
            ret = json.loads(r.text)
        except JSONDecodeError as e:
            return False
    else:
        return False
    return ret


def getbook(docid):
    url = 'https://resolver.kb.nl/resolve?urn=' + docid + ':pdf'

    r = requests.get(url)

    if r.status_code == 200:
        with open('./' + docid + '.pdf', 'wb') as f:  
            f.write(r.content)


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'article':
            for attr in attrs:
                if attr[0] == 'data-identifier':
                    docid = attr[1]
                    print("processing book ID: " + docid)
                    getbook(docid)
                if attr[0] == 'data-metadata':
                    docdata = json.loads(attr[1])
                    with open('./' + docdata['bookid'] + '.txt', 'w') as f:
                        f.write(json.dumps(docdata, indent=4))


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument('-a','--author',dest='author',help='auteur',type=str, required=True,nargs='?')
    parser.add_argument('-t','--title',dest='title',help='titel',type=str, required=True,nargs='?')

    args = parser.parse_args()

    stukkiejson = zoek(args.author,args.title)
    if stukkiejson:
        parser = MyHTMLParser()
        parser.feed(stukkiejson['resultsAction'])


if __name__ == "__main__":
       main(sys.argv[1:])
