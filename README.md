## Leech digitised books from the Koninklijke Bibliotheek

This script checks delpher.nl to see if there books are available there, based on an author and title parameter. When available, it downloads the PDF version and saves the metadata in a txt file.
